# List of Publications
---

|Name|Keywords|Publication|Presentation|Poster|Code|
|---|---|---|---|---|---|
|Harnessing the power of CNNs for unevenly-sampled light-curves using Markov Transition Field| *Markov Transition Field · Light Curves · Exoplanet Identification · Convolutional Neural Networks · Unevenly-Sampled Light Curves* | [:heavy_check_mark:](./manuscripts/2021_AC_MTF.pdf)| :x: | [:heavy_check_mark:](./posters/2020_MTF.pdf) | __[github](https://github.com/Buguemar/PIIC19/tree/master/code/obj1)__ |
|Learning to combine classifiers outputs with the transformer for text classification| *Unbalanced data · Transformer · Data Augmentation · Representation Learning · Text Classification* | [:heavy_check_mark:](./manuscripts/2020_IDA_Transf.pdf)| :x: | :x: | __[github](https://github.com/Buguemar/Transformer_as_ensemble)__ |
|Can we interpret machine learning? An analysis of exoplanet detection problem| *Machine Learning · Exoplanet* | [:heavy_check_mark:](./manuscripts/2019_ADASS_Exoplanet.pdf)| :x: | [:heavy_check_mark:](./posters/2019_ADASS_Exoplanet.pdf) | - |
|Learning to Detect Online Harassment on Twitter with the Transformer| *Harassment Detection · Self-attention Models · Social Media* | [:heavy_check_mark:](./manuscripts/2019_SIMAH_Transf.pdf)| [:heavy_check_mark:](./presentations/2019_SIMAH.pdf) | [:heavy_check_mark:](./posters/2019_SIMAH.pdf) | __[github](https://github.com/Buguemar/SIMAHcomp)__ |
|Classical machine learning techniques in the search of extrasolar planets| *Machine Learning · Exoplanet Detection · Feature Engineering · Light-curve* | [:heavy_check_mark:](./manuscripts/2019_CLEIEJ_Exoplanet.pdf)| :x: | :x: |  __[github](https://github.com/FMena14/ExoplanetDetection)__ |
|Applying Self-attention for Stance Classification| *Stance Classification · Self-attention Models · Social Networks* | [:heavy_check_mark:](./manuscripts/2019_CIARP_Transf.pdf)| [:heavy_check_mark:](./presentations/2019_CIARP.pdf) | :x:| __[github](https://github.com/Buguemar/Transformer_as_ensemble)__ |
|An empirical analysis of rumor detection on microblogs with recurrent neural networks| *Rumor Detection · Propagation Trees · Empirical Factors* | [:heavy_check_mark:](./manuscripts/2019_HCII_Rumor.pdf)| [:heavy_check_mark:](./presentations/2019_HCII.pdf)| :x: | - |
|Refining exoplanet detection using supervised learning and feature engineering| *Machine Learning · Exoplanet Detection · Feature Engineering* | [:heavy_check_mark:](./manuscripts/2018_CLEI_Exoplanet.pdf)| [:heavy_check_mark:](./presentations/2018_SLIOA-CLEI_Exoplanet.pdf) | [:heavy_check_mark:](./posters/2018_ChileWIC_exoplanet.pdf) |  __[github](https://github.com/FMena14/ExoplanetDetection)__ |
|ChiVOLabs: cloud service that offer interactive environment for reprocessing astronomical data| *Clouds · Astronomy· Observatories · Data storage · Prototyping · Computer architecture · Data centers* | [:heavy_check_mark:](./manuscripts/2018_SPIE_ChiVOLabs.pdf)| :x: | :x: | - |



[UP](#list-of-publications)

> __[Home](https://github.com/Buguemar/publications)__
