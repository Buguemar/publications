### Margarita Bugueno profile: __[github.com/Buguemar](https://github.com/Buguemar)__
---

# List of Publications
---

* [2021, journal] **Margarita Bugueño**, Gabriel Molina, Francisco Mena, Patricio Olivares, Mauricio Araya. *Harnessing the power of CNNs for unevenly-sampled light-curves using Markov Transition Field*. Astronomy and Computing. __[10.1016/j.ascom.2021.100461](https://doi.org/10.1016/j.ascom.2021.100461)__
    > [Manuscript](./manuscripts/2021_AC_MTF.pdf)  
    > [Supplementary Poster](./posters/2020_MTF.pdf)  
    
* [2020, journal] **Margarita Bugueño**, Marcelo Mendoza. *Learning to combine classifiers outputs with the transformer for text classification*. Intelligent Data Analysis, IOS Press. __[10.3233/IDA-200007](https://dx.doi.org/10.3233/IDA-200007)__
    > [Manuscript](./manuscripts/2020_IDA_Transf.pdf)  
 
* [2020, short-paper] Gabriel Molina, Francisco Mena, **Margarita Bugueño**, Mauricio Solar. *Can we interpret machine learning? An analysis of exoplanet detection problem*. Astronomical Data Analysis Software and Systems (ADASS), ASPCS. __[link](https://ui.adsabs.harvard.edu/abs/2020ASPC..527..183M/abstract)__
    > [Manuscript](./manuscripts/2019_ADASS_Exoplanet.pdf)  
    > [Poster](./posters/2019_ADASS_Exoplanet.pdf) 

* [2020, conference paper] **Margarita Bugueño**, Marcelo Mendoza. *Learning to Detect Online Harassment on Twitter with the Transformer*. Joint European Conference on Machine Learning and Knowledge Discovery in Databases (ECML-PKDD), Springer. __[10.1007/978-3-030-43887-6\_23](https://dx.doi.org/10.1007/978-3-030-43887-6\_23)__
    > [Manuscript](./manuscripts/2019_SIMAH_Transf.pdf)  
    > [Presentation](./presentations/2019_SIMAH.pdf)  
    > [Poster](./posters/2019_SIMAH.pdf) 

* [2019, journal] **Margarita Bugueño** & Francisco Mena, Mauricio Araya. *Classical machine learning techniques in the search of extrasolar planets*. CLEI Electronic Journal. __[10.19153/cleiej.22.3.3](https://dx.doi.org/10.19153/cleiej.22.3.3)__
    > [Manuscript](./manuscripts/2019_CLEIEJ_Exoplanet.pdf)  
    
* [2019, conference paper] **Margarita Bugueño**, Marcelo Mendoza. *Applying Self-attention for Stance Classification*. Ibero-american Congress on Pattern Recognition (CIARP), Springer. __[10.1007/978-3-030-33904-3\_5](https://dx.doi.org/10.1007/978-3-030-33904-3_5)__
    > [Manuscript](./manuscripts/2019_CIARP_Transf.pdf)  

* [2019, conference paper] **Margarita Bugueño**, Gabriel Sepúlveda, Marcelo Mendoza. *An empirical analysis of rumor detection on microblogs with recurrent neural networks*. International Conference on Human-Computer Interaction (HCII), Springer. __[10.1007/978-3-030-21902-4\_21](https://dx.doi.org/10.1007/978-3-030-21902-4\_21)__
    > [Manuscript](./manuscripts/2019_HCII_Rumor.pdf)  
    > [Presentation](./presentations/2019_HCII.pdf)  
    
* [2018, conference paper] **Margarita Bugueno**, Francisco Mena, Mauricio Araya. *Refining exoplanet detection using supervised learning and feature engineering*. Latin American Computer Conference (CLEI), IEEE. __[10.1109/CLEI.2018.00041](https://dx.doi.org/10.1109/CLEI.2018.00041)__
    > [Manuscript](./manuscripts/2018_CLEI_Exoplanet.pdf)  
    > [Presentation](./presentations/2018_SLIOA-CLEI_Exoplanet.pdf)  
    > [Supplementary Poster](./posters/2018_ChileWIC_exoplanet.pdf)  
    
* [2018, proceedings] Humberto Farias, Daniel Ortiz, Camilo Ñuñez, Mauricio Solar, **Margarita Bugueno**. *ChiVOLabs: cloud service that offer interactive environment for reprocessing astronomical data*. Software and Cyber infrastructure for Astronomy. __[10.1117/12.2313304](https://dx.doi.org/10.1117/12.2313304)__
    > [Manuscript](./manuscripts/2018_SPIE_ChiVOLabs.pdf)  
 
    
## Results Report
---
* Poster de ML y IA.. ademas de link al codigo github
    
---
[UP](#list-of-publications)

> Go to [Organized](./organization.md) details.
